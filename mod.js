const
    packageName = Deno.env.get('PACKAGE'),
    token = Deno.env.get('TOKEN'),
    result = [],
    fetchResults = async page => {
        const data = await (await fetch(`https://api.github.com/search/code?q=${packageName}+in:file+filename:package.json&per_page=100&page=${page}`, {
            headers: {
                'Authorization': `token ${token}`
            }
        })).json();
        result.push(...data['items'].map(item => ({
            repoName: item['repository']['full_name'],
            fileUrl: item['html_url']
        })));
        return data['total_count'];
    };
let resultCount, page = 1;
do { resultCount = await fetchResults(page++); }
while (result.length !== resultCount);
for(let i = result.length - 1; i > 0; i--){
    if(result.findIndex(item => item.repoName === result[i].repoName) !== i)
        result.splice(i, 1);
}
console.log(JSON.stringify(result, null, 4));